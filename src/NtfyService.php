<?php

namespace Drupal\ntfy;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Ntfy\Auth\Token;
use Ntfy\Auth\User;
use Ntfy\Client;
use Ntfy\Exception\NtfyException;
use Ntfy\Message;
use Ntfy\Server;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This service connects and sends notifications to a ntfy.sh server.
 */
class NtfyService implements NtfyServiceInterface {

  /**
   * The Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The ntfy.sh Server instance to use.
   *
   * @var \Ntfy\Server
   */
  protected Server $ntfyServer;

  /**
   * The ntfy client instance.
   *
   * @var \Ntfy\Client
   */
  protected Client $ntfyClient;

  /**
   * The auth object which either is a Token or User.
   *
   * @var \Ntfy\Auth\User|\Ntfy\Auth\Token|null
   */
  protected User|Token|null $ntfyAuth;

  /**
   * Constructs a NtfyService object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   *
   * @throws \Ntfy\Exception\NtfyException
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $ntfy_config = $config_factory->get('ntfy.settings');
    $ntfy_server = $ntfy_config->get('ntfy_server');
    if (is_string($ntfy_server)) {
      $this->ntfyServer = new Server($ntfy_server);
    }
    else {
      throw new NtfyException('Ntfy.sh server has not been set');
    }

    $this->setAuth($ntfy_config);
    $this->ntfyClient = new Client($this->getServer(), $this->getAuth());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    // @phpstan-ignore-next-line
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function sendNotification($topic, $title, $body): \stdClass {
    $message = new Message();
    $message->topic($topic);
    $message->title($title);
    $message->body($body);
    $message->priority(Message::PRIORITY_HIGH);
    return $this->ntfyClient->send($message);
  }

  /**
   * Set the auth for the ntfy.sh service.
   *
   * Depending on the auth method it will either create a \Ntfy\User or
   * \Ntfy\Token class instance.
   *
   * @param \Drupal\Core\Config\Config $ntfy_config
   *   The configuration for the module.
   */
  private function setAuth(Config $ntfy_config): void {
    // Authenticate via token or user/password.
    $auth_method = is_string(getenv('NTFY_AUTH_METHOD')) ? getenv('NTFY_AUTH_METHOD') : $ntfy_config->get('ntfy_authentication_method');

    $auth = NULL;
    if ($auth_method === 'token') {
      $access_token = is_string(getenv('NTFY_ACCESS_TOKEN')) ? getenv('NTFY_ACCESS_TOKEN') : $ntfy_config->get('ntfy_authentication_access_token');
      if (is_string($access_token)) {
        $auth = new Token($access_token);
      }
    }
    if ($auth_method === 'user') {
      $user = is_string(getenv('NTFY_USER')) ? getenv('NTFY_USER') : $ntfy_config->get('ntfy_authentication_user');
      $password = is_string(getenv('NTFY_PASSWORD')) ? getenv('NTFY_PASSWORD') : $ntfy_config->get('ntfy_authentication_password');
      if (is_string($user) && is_string($password)) {
        $auth = new User($user, $password);
      }
    }
    $this->ntfyAuth = $auth;
  }

  /**
   * {@inheritdoc}
   */
  public function getServer(): Server {
    return $this->ntfyServer;
  }

  /**
   * {@inheritdoc}
   */
  public function getClient(): Client {
    return $this->ntfyClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getAuth(): User|Token|null {
    return $this->ntfyAuth;
  }

}
