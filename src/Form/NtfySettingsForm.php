<?php

namespace Drupal\ntfy\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide the configuration form for the NtfyService.
 */
class NtfySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ntfy_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ntfy.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ntfy.settings');

    // Configure the ntfy.sh server instance.
    $form['ntfy_server'] = [
      '#type' => 'url',
      '#title' => $this->t('ntfy.sh Server'),
      '#default_value' => $config->get('ntfy_server') ?? 'https://ntfy.sh',
      '#description' => $this->t('Enter the ntfy.sh url for sending notifications.'),
    ];

    // Configure the default topic for sending notifications.
    $form['ntfy_default_topic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Notification Topic'),
      '#default_value' => $config->get('ntfy_default_topic'),
      '#description' => $this->t('Enter the default topic to send notifications to.'),
    ];

    // Allow for setting authentication details. Using environment variables
    // are recommended.
    $form['authentication'] = [
      '#type' => 'details',
      '#title' => $this->t('Authentication'),
      '#description' => $this->t('Enter authentication details if needed.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $env_auth_method = getenv('NTFY_AUTH_METHOD');

    $form['authentication']['method'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose the authentication method'),
      '#options' => [
        'token' => $this->t('Auth Token'),
        'user' => $this->t('Username and Password'),
      ],
      '#default_value' => is_string($env_auth_method) ? $env_auth_method : $config->get('ntfy_authentication_method'),
      '#disabled' => is_string(getenv('NTFY_AUTH_METHOD')),
    ];

    $auth_form_fields = [
      'access_token' => [
        '#type' => 'password',
        '#title' => $this->t('Access Token'),
        '#description' => $this->t('Enter the provided access token.'),
        '#states' => [
          'visible' => [
            'select[name="method"]' => ['value' => 'token'],
          ],
        ],
      ],
      'user' => [
        '#type' => 'textfield',
        '#title' => $this->t('User Name'),
        '#description' => $this->t('Provide the user to use for sending notifications.'),
        '#states' => [
          'visible' => [
            'select[name="method"]' => ['value' => 'user'],
          ],
        ],
      ],
      'password' => [
        '#type' => 'password',
        '#title' => $this->t('Password'),
        '#description' => $this->t('Provide the password of the user.'),
        '#states' => [
          'visible' => [
            'select[name="method"]' => ['value' => 'user'],
          ],
        ],
      ],
    ];

    foreach ($auth_form_fields as $config_key => $config_form_details) {
      $env_string = 'NTFY_' . strtoupper($config_key);
      if (is_string(getenv($env_string))) {
        $form['authentication'][$config_key] = [
          '#type' => 'textfield',
          '#title' => $config_form_details['#title'],
          '#description' => $config_form_details['#description'],
          '#default_value' => $this->t('This setting has been set with $_ENV["%env_variable_name"]', ["%env_variable_name" => $env_string]),
          '#disabled' => TRUE,
          '#states' => $config_form_details['#states'],
        ];
      }
      else {
        $form['authentication'][$config_key] = [
          '#type' => $config_form_details['#type'],
          '#title' => $config_form_details['#title'],
          '#description' => $config_form_details['#description'],
          '#default_value' => $config->get('ntfy_authentication_' . $config_key),
          '#states' => $config_form_details['#states'],
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    // Set ntfy settings.
    $this->config('ntfy.settings')
      ->set('ntfy_server', $form_state->getValue('ntfy_server'))
      ->set('ntfy_default_topic', $form_state->getValue('ntfy_default_topic'))
      ->save();

    // Set auth details.
    $auth_keys = [
      'method',
      'access_token',
      'user',
      'password',
    ];
    foreach ($auth_keys as $auth_key) {
      if (is_string($form_state->getValue($auth_key))) {
        $this->config('ntfy.settings')
          ->set('ntfy_authentication_' . $auth_key, $form_state->getValue($auth_key))
          ->save();
      }
    }
  }

}
