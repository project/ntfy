<?php

namespace Drupal\ntfy;

use Ntfy\Auth\Token;
use Ntfy\Auth\User;
use Ntfy\Client;
use Ntfy\Server;

/**
 * Interface for sending notifications to a ntfy.sh server instance.
 */
interface NtfyServiceInterface {

  /**
   * Send a notification to the ntfy.sh service.
   *
   * @param string $topic
   *   The topic to which the notification will be send.
   * @param string $title
   *   The title of the message.
   * @param string $body
   *   The message body of the message.
   *
   * @return \stdClass
   *   The response of the server.
   */
  public function sendNotification($topic, $title, $body): \stdClass;

  /**
   * Get the instance of the ntfy.sh server wrapper.
   *
   * @return \Ntfy\Server
   *   The instance for the server.
   */
  public function getServer(): Server;

  /**
   * Get the instance of the ntfy.sh client wrapper.
   *
   * @return \Ntfy\Client
   *   The instance for the client.
   */
  public function getClient(): Client;

  /**
   * Get the auth class instance.
   *
   * @return \Ntfy\Auth\User|\Ntfy\Auth\Token|null
   *   The correct auth instance depending on the auth method.
   */
  public function getAuth(): User|Token|null;

}
