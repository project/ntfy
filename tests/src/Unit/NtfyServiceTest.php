<?php

namespace Drupal\Tests\ntfy\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\ntfy\NtfyService;
use Drupal\Tests\UnitTestCase;
use Ntfy\Auth\Token;
use Ntfy\Auth\User;
use Ntfy\Client;
use Ntfy\Server;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test class for the \Drupal\ntfy\NtfyService service.
 */
class NtfyServiceTest extends UnitTestCase {

  /**
   * The modules to be installed for the test.
   *
   * @var array|string[]
   */
  protected static array $modules = ['ntfy'];

  /**
   * Test that the service has been created with token auth.
   *
   * @throws \Ntfy\Exception\NtfyException
   */
  public function testServiceCreationWithTokenAuth(): void {
    // Mock the ConfigFactoryInterface.
    $configFactory = $this->createMock(ConfigFactoryInterface::class);
    $configFactory->method('get')
      ->willReturnMap([
        ['ntfy.settings', $this->createConfigMockWithAccessToken()],
      ]);

    // Mock the ContainerInterface.
    $container = $this->createMock(ContainerInterface::class);
    $container->method('get')
      ->willReturn($configFactory);

    // Create the service.
    $ntfyService = new NtfyService($configFactory);

    // Assert that the service is created and the necessary objects are set.
    $this->assertInstanceOf(NtfyService::class, $ntfyService);
    $this->assertInstanceOf(Token::class, $ntfyService->getAuth());
    $this->assertInstanceOf(Server::class, $ntfyService->getServer());
    $this->assertInstanceOf(Client::class, $ntfyService->getClient());
  }

  /**
   * Test that the service has been created with user and password auth.
   *
   * @throws \Ntfy\Exception\NtfyException
   */
  public function testServiceCreationWithPasswordAuth(): void {
    // Mock the ConfigFactoryInterface.
    $configFactory = $this->createMock(ConfigFactoryInterface::class);
    $configFactory->method('get')
      ->willReturnMap([
        ['ntfy.settings', $this->createConfigMockWithUserPassword()],
      ]);

    // Mock the ContainerInterface.
    $container = $this->createMock(ContainerInterface::class);
    $container->method('get')
      ->willReturn($configFactory);

    // Create the service.
    $ntfyService = new NtfyService($configFactory);

    // Assert that the service is created and the necessary objects are set.
    $this->assertInstanceOf(NtfyService::class, $ntfyService);
    $this->assertInstanceOf(User::class, $ntfyService->getAuth());
    $this->assertInstanceOf(Server::class, $ntfyService->getServer());
    $this->assertInstanceOf(Client::class, $ntfyService->getClient());
  }

  /**
   * Create test config for token auth.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The mocked configuration.
   */
  protected function createConfigMockWithAccessToken(): MockObject {
    /** @var \PHPUnit\Framework\MockObject\MockObject $config */
    $config = $this->createMock(Config::class);
    $config->method('get')->willReturnMap([
      ['ntfy_server', 'https://ntfy.sh'],
      ['ntfy_authentication_method', 'token'],
      ['ntfy_authentication_access_token', '12345'],
    ]);
    return $config;
  }

  /**
   * Create test config for password auth.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The mocked configuration.
   */
  protected function createConfigMockWithUserPassword(): MockObject {
    /** @var \PHPUnit\Framework\MockObject\MockObject $config */
    $config = $this->createMock(Config::class);
    $config->method('get')->willReturnMap([
      ['ntfy_server', 'https://ntfy.sh'],
      ['ntfy_authentication_method', 'user'],
      ['ntfy_authentication_user', 'foo'],
      ['ntfy_authentication_password', 'bar'],
    ]);
    return $config;
  }

}
