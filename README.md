# ntfy.sh Integration module

## Features

This module connects Drupal with ntfy.sh and will send notifications to a
configured topic.

## Installation

Install as usual, recommend is to install via composer.

This module uses the ntfy PHP library from github.com [1]. Make sure to install
the library manually if you don't use composer.

## Usage

After installing the module head to /admin/config/system/ntfy and configure your
server settings.
You can either use your own ntfy.sh instance or the default one.

If your server requires authentication you can set the details as well. It is
recommended to use environment variables for the credentials.
The modules support authentication via access token or user/password.

### Environment variables

* NTFY_AUTH_METHOD -> Either "user" or "token"
* NTFY_ACCESS_TOKEN -> The access token for your user (only for "token" method)
* NTFY_USER -> The username on the ntfy.sh server instance (only for "user"
  method)
* NTFY_PASSWORD -> The password of the user on the ntfy.sh server instance (only
  for "user" method)

## Links

[1] https://github.com/VerifiedJoseph/ntfy-php-library
